ParaTask
========

ParaTask started life as a means of creating a GUI for command line tools.
However, I've found it increasingly useful for forms in JavaFX Applications.

A Task has a set of Parameters (hence the name). There are different types of
Parameters, such as a simple StringParameter, DoubleParameter etc, but there
are more complex ones too. Each parameter can have simple validation checks
(such as ranges for integer values). In addition, the Task itself can perform
addition validation (cross checking parameters which have dependencies).
If all the checks pass, then the task is run.

The values for the parameters can be parsed from a command line, or entered
using a graphical interface. The GUI can be a stand-alone window, or be
embedded within a part of your application. 


Build
-----

The prerequisites are a Java JDK (including javaFX) and gradle v2.0 or higher.

For Debian Linux (as root) :

    apt-get install openjdk-8-jdk libopenjfx-java gradle

Note. If you are running an old version of Debian (e.g. Jessie), you will need to download gradle v2+ manually because
Jessie only supports gradle version 1.5, and I believe version 2.0+ is required.

    git clone https://gitlab.com/nickthecoder/paratask.git
    cd paratask
    gradle


