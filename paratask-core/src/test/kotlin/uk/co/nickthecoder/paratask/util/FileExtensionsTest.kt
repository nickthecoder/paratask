package uk.co.nickthecoder.paratask.util

import org.junit.Assert.assertEquals
import org.junit.Test

class FileExtensionsTest {

    val cd = if (currentDirectory.name == "paratask-core") currentDirectory else currentDirectory.child("paratask-core")
    val base = cd.child("src", "test", "resources", "FileExtensions")

    @Test
    fun childTest() {
        assertEquals("FileExtensions", base.name)
        assertEquals("resources", base.parentFile.name)
        assertEquals("test", base.parentFile.parentFile.name)
        assertEquals("src", base.parentFile.parentFile.parentFile.name)
        assertEquals(cd, base.parentFile.parentFile.parentFile.parentFile)
    }
}
