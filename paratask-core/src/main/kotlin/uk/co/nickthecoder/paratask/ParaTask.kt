/*
ParaTask Copyright (C) 2017  Nick Robinson>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.paratask

import javafx.scene.Scene
import javafx.scene.image.Image

object ParaTask {

    private val imageMap = mutableMapOf<String, Image?>()

    val cssUrl = this::class.java.getResource("paratask.css")?.toExternalForm()

    fun style(scene: Scene) {
        scene.stylesheets.add(cssUrl)
    }

    fun imageResource(name: String): Image? {
        val image = imageMap[name]
        if (image == null) {
            val inputStream = ParaTask::class.java.getResourceAsStream(name)
            val newImage = if (inputStream == null) null else Image(inputStream)
            imageMap[name] = newImage
            inputStream?.close()
            return newImage
        }
        return image
    }
}
